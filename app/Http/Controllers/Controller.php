<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    protected function buildFailedValidationResponse(Request $request, array $errors) {
        $message = '';
        foreach ($errors as $key => $value) {
            $message = $value[0];
            break;
        }
        return ["success" => false, "code"=> 422, "message" => $message, "errors" => $errors];
    }
}
