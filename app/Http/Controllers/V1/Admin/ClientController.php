<?php

namespace App\Http\Controllers\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $query = Client::query();

        if($request->has('name')){
            $query->where('name', $request->get('name'));
        }

        $data = $query->paginate($request->get('per_page', 10));

        if (!$data->isEmpty()) {
            $response = [
                'success'   => true,
                'code'      => 200,
                'data'      => $data
            ];
        }else{
            $response = [
                'success'   => false,
                'code'      => 404,
                'data'      => []
            ];
        }

        return response()->json($response, $response['code']);
    }

    public function show($id)
    {
        $data = Client::find($id);

        if ($data) {
            $response = [
                'success'   => true,
                'code'      => 200,
                'data'      => $data
            ];
        }else{
            $response = [
                'success'   => false,
                'code'      => 404,
                'data'      => []
            ];
        }

        return response()->json($response, $response['code']);
    }

    public function store(Request $request)
    {
        $input = $this->validate($request, [
            'name' => 'required|string|max:200',
            'address' => 'required|string|max:200',
            'region' => 'required|string|max:200',
            'total_account' => 'required|integer|digits_between:1,11',
            'duration'  => 'required|integer|digits_between:1,11',
            'admin_name'  => 'required|string|max:200',
            'admin_email'  => 'required|email|unique:users,email',
            'admin_password'  => 'required|string|min:6|max:50',
        ]);
        try {
            DB::beginTransaction();
            $user = User::create([
                'name'      => $input['admin_name'],
                'email'     => $input['admin_email'],
                'password'  => Hash::make($input['admin_password'])
            ]);

            $client = Client::create([
                'name'      => $input['name'],
                'address'   => $input['address'],
                'region'    => $input['region'],
                'total_account' => $input['total_account'],
                'duration'      => $input['duration'],
                'admin_id'      => $user->id
            ]);

            DB::commit();
            $response = [
                'success'   => true,
                'code'      => 201,
                'data'      => $client
            ];
        } catch (\Throwable $th) {
            DB::rollBack();
            $response = [
                'success'   => false,
                'code'      => 403,
                'data'      => []
            ];
        }

        return response()->json($response, $response['code']);
    }

    public function update(Request $request, $id)
    {
        $input = $this->validate($request, [
            'name' => 'required|string|max:200',
            'address' => 'required|string|max:200',
            'region' => 'required|string|max:200',
            'total_account' => 'required|integer|digits_between:1,11',
            'duration'  => 'required|integer|digits_between:1,11'
        ]);

        $data = Client::where('id', $id)->first();

        if($data){
            try {
                $update = $data->update([
                    'name'      => $input['name'],
                    'address'   => $input['address'],
                    'region'    => $input['region'],
                    'total_account' => $input['total_account'],
                    'duration'      => $input['duration']
                ]);
                $response = [
                    'success'   => true,
                    'code'      => 200,
                    'data'      => $data
                ];
            } catch (\Throwable $th) {
                $response = [
                    'success'   => false,
                    'code'      => 403,
                    'message'   => 'Gagal memperbarui data',
                    'errors'    => []
                ];
            }
        }else{
            $response = [
                'success'   => false,
                'code'      => 404,
                'message'   => 'Data tidak ditemukan',
                'errors'    => []
            ];
        }

        return response()->json($response, $response['code']);
    }

    public function delete($id)
    {
        $data = Client::where('id', $id)->first();

        if($data){
            try {
                $delete = $data->delete();
                $response = [
                    'success'   => true,
                    'code'      => 200,
                    'data'      => []
                ];
            } catch (\Throwable $th) {
                $response = [
                    'success'   => false,
                    'code'      => 403,
                    'message'   => 'Gagal menghapus data',
                    'errors'    => []
                ];
            }
        }else{
            $response = [
                'success'   => false,
                'code'      => 404,
                'message'   => 'Data tidak ditemukan',
                'errors'    => []
            ];
        }

        return response()->json($response, $response['code']);
    }
}
