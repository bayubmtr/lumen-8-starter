<?php

namespace App\Http\Controllers\V1\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use \Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => [
            'login'
        ]]);
    }

    public function login(Request $request)
    {
        $credentials = request(['email', 'password']);

        if ($token = Auth::attempt($credentials)) {
            $response = [
                'success'   => true,
                'data'      => [
                    'user'  => auth()->user(),
                    'token' => $token
                ],
                'code'      => 200
            ];
        }else{
            $response = [
                'success'   => false,
                'data'      => [
                    'user'  => [],
                    'token' => null
                ],
                'code'      => 403
            ];
        }
        return response()->json($response, $response['code']);
    }

    public function logout(Request $request)
    {
        JWTAuth::invalidate(JWTAuth::getToken());

        $response = [
            'success'   => true,
            'data'      => [],
            'code'      => 200
        ];

        return response()->json($response, $response['code']);
    }
}
