<?php

return [
    /*
    |---------------------------------------------------------------------------------------
    | Baris Bahasa untuk Pengingat Kata Sandi
    |---------------------------------------------------------------------------------------
    |
    | Baris bahasa berikut adalah baris standar yang cocok dengan alasan yang
    | diberikan oleh pembongkar kata sandi yang telah gagal dalam upaya pembaruan
    | kata sandi, misalnya token tidak valid atau kata sandi baru tidak valid.
    |
    */

    'bank-partners.index'   => 'Data Mitra Bank',
    'residents.index'       => 'Data Warga',
    'clusters.index'        => 'Data Cluster',
    'ipl-payments.index'    => 'Data Pembayaran IPL',
    'accounts.index'        => 'Data Account User',
    'ipl-rates.index'       => 'Data Tarif IPL'
];
