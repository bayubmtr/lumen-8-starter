<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['namespace' => 'V1', 'prefix' => 'v1'], function () use ($router) {
    $router->group(['namespace' => 'Auth'], function () use ($router) {
        $router->post('/login', 'AuthController@login');
        $router->post('/logout', 'AuthController@logout');
    });

    $router->group(['namespace' => 'Admin', 'prefix' => 'admin'], function () use ($router) {
        $router->get('/clients', 'ClientController@index');
        $router->post('/clients', 'ClientController@store');
        $router->get('/clients/{id}', 'ClientController@show');
        $router->patch('/clients/{id}', 'ClientController@update');
        $router->delete('/clients/{id}', 'ClientController@delete');
    });
});
